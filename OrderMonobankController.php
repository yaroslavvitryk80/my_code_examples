<?php
declare(strict_types=1);

namespace App\Controller\Endpoint\OrderMonobank;

use App\Component\Signature\OpenSslSignature;
use App\Component\Traits\EntityManagerAwareTrait;
use App\Component\Traits\LoggerAwareTrait;
use App\Controller\AbstractApiController;
use App\Domain\Services\Directory\Order\OrderService;
use App\Domain\Services\Processing\DefaultProcessingService;
use App\Entity\Directory\OrderMonobank;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use App\Domain\Services\Processing\ReceiptProcessingService;
use App\Domain\Services\Processing\ProcessingService;

/**
 * @Route("/monobank/", name="monobank_")
 */
class OrderMonobankController extends AbstractApiController
{
    use EntityManagerAwareTrait;
    use LoggerAwareTrait;

    private const CREATED_INVOICE_STATUS = 'created';
    private const PROCESSING_INVOICE_STATUS = 'processing';
    private const SUCCESS_INVOICE_STATUS = 'success';
    private const FAILURE_INVOICE_STATUS = 'failure';
    private const REVERSED_INVOICE_STATUS = 'reversed';

    private OpenSslSignature $openSslSignature;
    private OrderService $orderService;

    public function __construct(
        OpenSslSignature $openSslSignature,
        OrderService $orderService
    ) {
        $this->openSslSignature = $openSslSignature;
        $this->orderService = $orderService;
    }

    /**
     * @Route("payment", name="payment", methods={"POST"})
     */
    public function monobankPaymentAction(Request $request): Response
    {
        $sign = $request->query->get('sign');
        $content = json_decode($request->getContent(), true);
        $this->logger->info(sprintf('Payment from Monobank: %s', json_encode([
            'body' => $content,
            'headers' => $request->headers->all()
        ])));

        $referenceId = $content['reference'];

        $fixedSign = str_replace(' ', '+', $sign);

        if (!$this->openSslSignature->verify($fixedSign, $referenceId)) {
            $this->logger->warning('Payment from monobank: invalid sign: ' . $sign);
            throw new BadRequestHttpException('Invalid sign');
        }

        $order = $this->entityManager->getRepository(OrderMonobank::class)->findOneBy(['referenceId' => $referenceId]);

        if (!($order instanceof OrderMonobank)) {
            $this->logger->error("Payment from monobank: order with reference $referenceId not found");
            throw new NotFoundHttpException('Order not found');
        }
        if(!in_array($order->getStatus(), [OrderMonobank::STATUS_FAILURE, OrderMonobank::STATUS_SUCCESS])){
            try {
                $this->orderService->processOrder($order, $content);
            } catch (\Exception $e){
                $message = $e->getMessage();
                $this->logger->error(sprintf('Error when trying to create receipt for monobank order: %s, error: %s (скоріше за все не вдалось видати чек бо зміна вже закрита)', $order->getId()->toString(), $message));
                $order->setFailureReason($e->getMessage() . " (скоріше за все не вдалось видати чек бо зміна вже закрита)");
                $this->entityManager->flush();
            }
        } else {
            $this->logger->warning(sprintf('Monobank order with id %s already processed', $order->getId()->toString()));
        }

        return new Response('', 200);
    }
}
