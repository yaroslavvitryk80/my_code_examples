<?php
declare(strict_types=1);

namespace App\Entity\Directory;

use App\Entity\Interfaces\OrganizationRestrictedInterface;
use App\Entity\Traits\OrganizationRestrictedTrait;
use App\Entity\Traits\Timestampable;
use App\Entity\Traits\WithUuid;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use App\Controller\Endpoint\OrderMonobank\CreateOrderMonobankAction;
use App\Component\Filters\IOrderFilter;
use ApiPlatform\Core\Annotation\ApiFilter;

/**
 * Class OrderMonobank.
 *
 * @ORM\Entity()
 * @ORM\Table(schema="directory", name="order_monobanks")
 * @ORM\HasLifecycleCallbacks()
 * @ApiResource(
 *      normalizationContext={"groups"={"order_mono_list"}},
 *      denormalizationContext={"groups"={"order_mono_create"}},
 *      collectionOperations={
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"={"groups"={"order_mono_list"}}
 *          },
 *          "post"={
 *              "method"="POST",
 *              "controller"=CreateOrderMonobankAction::class,
 *              "denormalization_context"={"groups"={"order_mono_create"}}
 *          }
 *      },
 *      itemOperations={
 *          "get",
 *          "put"={
 *              "method"="PUT",
 *              "denormalization_context"={"groups"={"order_mono_update"}}
 *          }
 *      }
 * )
 * @ApiFilter(
 *     IOrderFilter::class,
 *     properties={
 *          "dateCreated"
 *     }
 * )
 */
class OrderMonobank implements OrganizationRestrictedInterface
{
    use WithUuid;
    use Timestampable;

    public const STATUS_CREATED = 'created';
    public const STATUS_SUCCESS = 'success';
    public const STATUS_FAILURE = 'error';
    // public const STATUS_REVERSED = 'reversed';

    /**
     * @ORM\Column(type="json", nullable=false)
     *
     * @Assert\NotBlank(groups={"order_mono_create"})
     *
     * @Groups({"order_mono_create", "order_mono_list"})
     */
    private ?array $receiptBody = null;

    /**
     * @ORM\Column(type="integer", nullable=false)
     *
     * @Groups({"order_mono_list"})
     */
    private ?int $totalSum = null;

    /**
     * @ORM\Column(type="text", nullable=false)
     *
     * @Assert\Type(type="string")
     *
     * @Groups({"order_mono_list"})
     */
    private ?string $invoiceId = null;

    /**
     * @ORM\Column(type="text", nullable=false)
     *
     * @Assert\Type(type="string")
     *
     * @Groups({"order_mono_list"})
     */
    private ?string $pageUrl = null;

    /**
     * @ORM\Column(type="text", nullable=true, length=36)
     *
     * @Assert\Type(type="string")
     * @Groups({"order_mono_list", "order_mono_update"})
     */
    private ?string $receiptId = null;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private ?array $receiptResponse = null;

    /**
     * @ORM\Column(type="text", nullable=false, length=36)
     */
    private ?string $referenceId = null;

    /**
     * @ORM\ManyToOne(targetEntity=Employee::class, inversedBy="orderMonobank")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     *
     * @Assert\NotBlank(groups={"order_mono_create"})
     *
     * @Groups({"order_mono_create", "order_mono_list"})
     */
    private ?Employee $employee = null;

    /**
     * @ORM\ManyToOne(targetEntity=CashRegister::class, inversedBy="orderMonobank")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     *
     * @Assert\NotBlank(groups={"order_mono_create"})
     *
     * @Groups({"order_mono_create", "order_mono_list"})
     */
    private ?CashRegister $cashRegister = null;

    /**
     * @ORM\Column(name="status", type="string", length=10, nullable=false, options={"default": "CREATED"})
     *
     * @Assert\NotBlank()
     * @Assert\Type(type="string")
     *
     * @Groups({"order_mono_list"})
     */
    private string $status = self::STATUS_CREATED;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @Groups({"order_mono_list"})
     *
     */
    private ?string $failureReason = null;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private ?\DateTimeInterface $payedAt = null;

    public function getReceiptBody(): ?array
    {
        return $this->receiptBody;
    }

    public function setReceiptBody(array $receiptBody): self
    {
        $this->receiptBody = $receiptBody;
        return $this;
    }

    public function getInvoiceId(): ?string
    {
        return $this->invoiceId;
    }

    public function setInvoiceId(?string $invoiceId): self
    {
        $this->invoiceId = $invoiceId;
        return $this;
    }

    public function getPageUrl(): ?string
    {
        return $this->pageUrl;
    }

    public function setPageUrl(?string $pageUrl): self
    {
        $this->pageUrl = $pageUrl;
        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;
        return $this;
    }

    public function getTotalSum(): ?int
    {
        return $this->totalSum;
    }

    public function setTotalSum(int $totalSum): self
    {
        $this->totalSum = $totalSum;
        return $this;
    }

    public function getReceiptId(): ?string
    {
        return $this->receiptId;
    }

    public function setReceiptId(?string $receiptId): self
    {
        $this->receiptId = $receiptId;

        return $this;
    }

    public function getReferenceId(): ?string
    {
        return $this->referenceId;
    }

    public function setReferenceId(string $referenceId): self
    {
        $this->referenceId = $referenceId;
        return $this;
    }

    public function getEmployee(): ?Employee
    {
        return $this->employee;
    }

    public function setEmployee(Employee $employee): self
    {
        $this->employee = $employee;
        return $this;
    }

    public function getCashRegister(): ?CashRegister
    {
        return $this->cashRegister;
    }

    public function setCashRegister(CashRegister $cashRegister): self
    {
        $this->cashRegister = $cashRegister;
        return $this;
    }

    public function getFailureReason(): ?string
    {
        return $this->failureReason;
    }

    public function setFailureReason(string $failureReason): self
    {
        $this->failureReason = $failureReason;
        return $this;
    }

    public function getPayedAt(): ?\DateTimeInterface
    {
        return $this->payedAt;
    }

    public function setPayedAt(?\DateTimeInterface $payedAt): self
    {
        $this->payedAt = $payedAt;
        return $this;
    }

    public function getReceiptResponse(): ?array
    {
        return $this->receiptResponse;
    }

    public function setReceiptResponse(?array $receiptResponse): self
    {
        $this->receiptResponse = $receiptResponse;
        return $this;
    }

    public static function getOrganizationPropertyPath(): string
    {
        return 'cashRegister.organization';
    }
}
