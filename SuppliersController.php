<?php
declare(strict_types=1);

namespace App\Controller\Accounting;

use ApiPlatform\Core\Bridge\Symfony\Validator\Exception\ValidationException;
use App\Domain\Services\AccountingSystem\Goods\GoodsGroups;
use App\Domain\Services\AccountingSystem\Suppliers\Suppliers;
use App\Domain\Services\AccountingSystem\Suppliers\Supplies;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Domain\Services\AccountingSystem\Goods\Goods;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Route("/accounting", name="accounting_")
 */
class SuppliersController extends AbstractGoodsAccountingController
{
    private Suppliers $suppliersService;
    private Supplies $suppliesService;

    public function __construct(ValidatorInterface $validator, Suppliers $suppliersService, Supplies $suppliesService)
    {
        $this->suppliersService = $suppliersService;
        $this->suppliesService = $suppliesService;
        parent::__construct($validator);
    }

    /**
     * @Route("/suppliers", name="get-suppliers-list", methods={"GET"})
     */
    public function getSuppliersList(Request $request)
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");
        $content = $request->query->all();

        return new Response(json_encode($this->suppliersService->getList($content)));
    }

    /**
     * @Route("/suppliers", name="create-suppliers", methods={"POST"})
     */
    public function postSuppliers(Request $request)
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");
        $content = json_decode($request->getContent(), true);
        $this->validateSuppliersPayload($content);

        return $this->suppliersService->create($content);
    }

    /**
     * @Route("/supplies", name="get-supplies-list", methods={"GET"})
     */
    public function getSuppliesList(Request $request)
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");
        $content = $request->query->all();

        return new Response(json_encode($this->suppliesService->getList($content)));
    }

    /**
     * @Route("/supplies", name="create-multiply-supplies", methods={"POST"})
     */
    public function postMultiplySupplies(Request $request)
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");
        $content = json_decode($request->getContent(), true);
        $this->validateMultiplySuppliesPayload($content);

        return $this->suppliesService->create($content, '');
    }

    /**
     * @Route("/supplies/{good_id}", name="create-supplies", methods={"POST"})
     */
    public function postSupplies(string $good_id, Request $request)
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");
        $content = json_decode($request->getContent(), true);
        $this->validateSuppliesPayload($content);

        return $this->suppliesService->create($content, $good_id);
    }

    /**
     * @Route("/supplies/{id}/correct", name="put-supplies", methods={"PUT"})
     */
    public function putSupplies(string $id, Request $request)
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");
        $content = json_decode($request->getContent(), true);
        $this->validateSuppliesPayloadOnUpdate($content);

        return $this->suppliesService->edit($id, $content);
    }

    private function validateSuppliersPayload(?array $content)
    {
        $violations = $this->validator->validate($content ?? [], new Assert\Collection([
            'name' => [new Assert\NotBlank(), new Assert\Type('string')],
            'description' => [new Assert\Optional(new Assert\Type('string'))]
        ]));

        if (0 !== count($violations)) {
            throw new ValidationException($violations);
        }
    }

    private function validateMultiplySuppliesPayload(?array $content)
    {
        $violations = $this->validator->validate($content ?? [], new Assert\All(new Assert\Collection([
            'purchase_price' => [new Assert\NotBlank(), new Assert\Type('integer')],
            'count' => [new Assert\NotBlank(), new Assert\Type('integer')],
            'date' => [new Assert\NotBlank(), new Assert\Type('string')],
            'supplier_id' => [new Assert\NotBlank(), new Assert\Uuid()],
            'comment' => [new Assert\Optional(new Assert\Type('string'))],
            'receipt_id' => [new Assert\Optional(new Assert\Uuid())],
            'goods_id' => [new Assert\NotBlank(), new Assert\Uuid()]
        ])));

        if (0 !== count($violations)) {
            throw new ValidationException($violations);
        }
    }

    private function validateSuppliesPayload(?array $content)
    {
        $violations = $this->validator->validate($content ?? [], new Assert\Collection([
            'purchase_price' => [new Assert\NotBlank(), new Assert\Type('integer')],
            'count' => [new Assert\NotBlank(), new Assert\Type('integer')],
            'date' => [new Assert\NotBlank(), new Assert\Type('string')],
            'supplier_id' => [new Assert\NotBlank(), new Assert\Uuid()],
            'comment' => [new Assert\Optional(new Assert\Type('string'))],
            'receipt_id' => [new Assert\Optional(new Assert\Uuid())]
        ]));

        if (0 !== count($violations)) {
            throw new ValidationException($violations);
        }
    }

    private function validateSuppliesPayloadOnUpdate(?array $content)
    {
        $violations = $this->validator->validate($content ?? [], new Assert\Collection([
            'purchase_price' => [new Assert\Optional(new Assert\Type('integer'))],
            'count' => [new Assert\Optional(new Assert\Type('integer'))],
            'comment' => [new Assert\Optional(new Assert\Type('string'))],
            'receipt_id' => [new Assert\Optional(new Assert\Type('string'))]
        ]));

        if (0 !== count($violations)) {
            throw new ValidationException($violations);
        }
    }
}
