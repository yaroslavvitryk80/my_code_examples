<?php
declare(strict_types=1);

namespace App\Entity\Directory;

use App\Entity\Interfaces\OrganizationRestrictedInterface;
use App\Entity\Traits\OrganizationRestrictedTrait;
use App\Entity\Traits\WithUuid;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(schema="directory", name="receipt_settings")
 * @ORM\HasLifecycleCallbacks()
 * @ApiResource(
 *      attributes={"security"="is_granted('ROLE_CASHIER')"},
 *      normalizationContext={"groups"={"receiptSettings_read", "receiptSettings_list"}},
 *      denormalizationContext={"groups"={"receiptSettings_create", "receiptSettings_update"}},
 *      collectionOperations={
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"={"groups"={"receiptSettings_list"}}
 *          },
 *          "post"={
 *              "method"="POST",
 *              "validation_groups"={"receiptSettings_create"},
 *              "denormalization_context"={"groups"={"receiptSettings_create"}}
 *          }
 *      },
 *      itemOperations={
 *          "get"={
 *              "method"="GET",
 *              "normalization_context"={"groups"={"receiptSettings_read"}}
 *          },
 *          "put"={
 *              "method"="PUT",
 *              "validation_groups"={"receiptSettings_update"},
 *              "denormalization_context"={"groups"={"receiptSettings_update"}},
 *          }
 *     }
 * )
 */

class ReceiptSettings implements OrganizationRestrictedInterface
{
    use WithUuid;
    use OrganizationRestrictedTrait;

    /**
     * @ORM\OneToOne(targetEntity="Organization", inversedBy="receiptSetting")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id")
     *
     * @Groups({"receiptSettings_read", "receiptSettings_create", "receiptSettings_list", "org_update"})
     */
    public ?Organization $organization = null;

    /**
     * @ORM\Column(type="integer", nullable=false)
     * @Assert\Range(
     *      min = 10,
     *      max = 250,
     *      groups={"receiptSettings_create", "receiptSettings_update"}
     * )
     * @Assert\NotNull(groups={"receiptSettings_create", "receiptSettings_update"})
     * @Groups({"receiptSettings_read", "receiptSettings_create", "receiptSettings_update", "receiptSettings_list", "org_list", "org_update"})
     */
    private ?int $width = null;

    /**
     * @ORM\Column(type="integer", nullable=false, options={"default":58})
     *  @Assert\Range(
     *      min = 40,
     *      max = 80,
     *      groups={"receiptSettings_update"}
     * )
     *
     * @Assert\NotNull(groups={"receiptSettings_update"})
     * @Groups({"org_read", "org_list", "org_create", "org_update", "receiptSettings_read", "receiptSettings_list", "receiptSettings_create", "receiptSettings_update"})
     */
    private int $paperWidth = 58;

    public function getOrganization(): ?Organization
    {
        return $this->organization;
    }

    public function setOrganization(?Organization $organization): void
    {
        $this->organization = $organization;
    }

    public function getWidth(): ?int
    {
        return $this->width;
    }

    public function setWidth(int $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getPaperWidth(): int
    {
        return $this->paperWidth;
    }

    public function setPaperWidth(int $paperWidth): self
    {
        $this->paperWidth = $paperWidth;
        return $this;
    }
}
