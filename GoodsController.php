<?php
declare(strict_types=1);

namespace App\Controller\Accounting;

use ApiPlatform\Core\Bridge\Symfony\Validator\Exception\ValidationException;
use App\Domain\Services\AccountingSystem\Goods\GoodsGroups;
use App\Entity\Directory\Tax;
use Doctrine\ORM\AbstractQuery;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Domain\Services\AccountingSystem\Goods\Goods;
use Symfony\Component\Validator\Constraints as Assert;
use App\Component\Traits\EntityManagerAwareTrait;
use App\Component\Utils\TempFile;

/**
 * @Route("/accounting", name="accounting_")
 */
class GoodsController extends AbstractGoodsAccountingController
{
    use EntityManagerAwareTrait;

    private Goods $goodsService;
    private GoodsGroups $goodsGroupsService;

    public function __construct(ValidatorInterface $validator, Goods $goodsService, GoodsGroups $goodsGroupsService)
    {
        $this->goodsService = $goodsService;
        $this->goodsGroupsService = $goodsGroupsService;
        parent::__construct($validator);
    }

    /**
     * @Route("/goods", name="create-goods", methods={"POST"})
     */
    public function postGoods(Request $request)
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");
        $content = json_decode($request->getContent(), true);
        $this->validateGoodsCreate($content);

        return $this->goodsService->create($content);
    }

    /**
     * @Route("/goods", name="get-goods-list", methods={"GET"})
     */
    public function getGoodsList(Request $request)
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");
        $content = $request->query->all();

        return new Response(json_encode($this->goodsService->getList($content)));
    }

    /**
     * @Route("/goods/{id}", name="get-goods-by-id", methods={"GET"})
     */
    public function getGoodsById(string $id)
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");
        return $this->goodsService->getItem($id);
    }

    /**
     * @Route("/goods/{id}", name="put-goods", methods={"PUT"})
     */
    public function putGoods(string $id, Request $request)
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");
        $content = json_decode($request->getContent(), true);
        $this->validateGoodsUpdate($content);
        return $this->goodsService->edit($id, $content);
    }

    /**
     * @Route("/goods/write-off", name="create-goods-write-off", methods={"POST"})
     */
    public function postGoodsWriteOff(Request $request)
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");
        $content = json_decode($request->getContent(), true);
        $this->validateGoodsWriteOffCreate($content);

        return $this->goodsService->createWriteOff($content);
    }

    /**
     * @Route("/goods-groups", name="create-goods-groups", methods={"POST"})
     */
    public function postGroups(Request $request)
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");
        $content = json_decode($request->getContent(), true);
        $this->validateGroupsCreate($content);

        return $this->goodsGroupsService->create($content);
    }

    /**
     * @Route("/goods-groups", name="get-goods-groups-list", methods={"GET"})
     */
    public function getGroupsList(Request $request)
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");
        $content = $request->query->all();

        return new Response(json_encode($this->goodsGroupsService->getList($content)));

    }

    /**
     * @Route("/goods-groups/{id}", name="edit-goods-group", methods={"PUT"})
     */
    public function putGroups(string $id, Request $request)
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");
        $content = json_decode($request->getContent(), true);
        $this->validateGroupsUpdate($content);
        return $this->goodsGroupsService->edit($id, $content);
    }

    /**
     * @Route("/goods/{good_id}/supplies", name="create-goods-supplies", methods={"POST"})
     */
    public function postSupplies(string $good_id, Request $request)
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");
        $content = json_decode($request->getContent(), true);
        $this->validateGoodsSuppliesPayload($content);

        return $this->goodsService->createSupply($good_id, $content);
    }

    /**
     * @Route("/upload/goods/csv", name="upload-csv-file", methods={"POST"})
     */
    public function uploadCsv(Request $request) : Response
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");
        $file = $request->files->get('file');
        if(!$file){
            throw new BadRequestHttpException('Файл для завантаження э обов\'язковим');
        }
        $fileStream = fopen($file->getPathName(), 'r');

        return $this->goodsService->uploadCsv($fileStream, json_encode($this->getTaxes()));
    }

    /**
     * @Route("/upload/goods/excel", name="upload-excel-file", methods={"POST"})
     */
    public function uploadExcel(Request $request): Response
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");
        $file = $request->files->get('file');
        if(!$file){
            throw new BadRequestHttpException('Файл для завантаження э обов\'язковим');
        }
        $fileStream = fopen($file->getPathName(), 'r');

        return $this->goodsService->uploadExcel($fileStream, json_encode($this->getTaxes()));
    }

    /**
     * @Route("/upload/goods/{id}", name="check-status-file", methods={"GET"})
     */
    public function checkUploadStatus(string $id): Response
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");

        return $this->goodsService->checkUploadStatus($id);
    }

    /**
     * @Route("/upload/goods/{id}", name="approve-goods-file", methods={"POST"})
     */
    public function approveUploadStatus(string $id): Response
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");

        return $this->goodsService->approveUpload($id);
    }

    /**
     * @Route("/upload/goods/example/{example}", name="get-upload-example", methods={"GET"})
     */
    public function getExample(string $example): Response
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");

        if($example === 'csv'){
            return $this->goodsService->getCsvExample();
        } else if($example === 'excel'){
            return $this->goodsService->getExcelExample();
        }
        throw new BadRequestHttpException('Wrong file type');
    }

    /**
     * @Route("/export/goods/csv", name="export-csv-file", methods={"POST"})
     */
    public function exportCsv(Request $request) : Response
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");
        return $this->goodsService->exportCsv(json_encode($this->getTaxes()));
    }

    /**
     * @Route("/export/goods/excel", name="export-excel-file", methods={"POST"})
     */
    public function exportExcel(Request $request) : Response
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");
        return $this->goodsService->exportExcel(json_encode($this->getTaxes()));
    }

    /**
     * @Route("/export/goods/{id}", name="check-export-status-file", methods={"GET"})
     */
    public function checkExportStatus(string $id): Response
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");

        return $this->goodsService->checkExportStatus($id);
    }

    /**
     * @Route("/export/goods/file/{id}", name="get-export-file", methods={"POST"})
     */
    public function getExportFile(string $id): Response
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");

        $fileFromFrontSystem = $this->goodsService->getExportFile($id);

        $file = (new TempFile())->createNewFile()
            ->write($fileFromFrontSystem->getContent());

        $resource = fopen($file->getUri(), 'r');

        return $this->fromFile($resource);
    }

    /**
     * @Route("/goods/{id}", name="delete-goods", methods={"DELETE"})
     */
    public function deleteGoods(string $id): Response
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");
        return $this->goodsService->deleteItem($id);
    }

    /**
     * @Route("/goods", name="delete-all-goods", methods={"DELETE"})
     */
    public function deleteAllGoods(Request $request): Response
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");
        $groupId = $request->query->get('groupId');

        return $this->goodsService->deleteAll($groupId);
    }

    /**
     * @Route("/goods-groups/{id}", name="delete-group", methods={"DELETE"})
     */
    public function deleteGoodsGroup(string $id): Response
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");
        return $this->goodsGroupsService->deleteItem($id);
    }

    /**
     * @Route("/goods-groups/{id}/move-goods", name="move-goods-to-other-group", methods={"PUT"})
     */
    public function moveGoodsFromGroup($id, Request $request): Response
    {
        $this->denyAccessUnlessGranted("ROLE_CASHIER");
        $payload = json_decode($request->getContent(), true);

        return$this->goodsGroupsService->moveGoodsTo($id, $payload['toGroup']);

    }

    private function validateGoodsSuppliesPayload(?array $content)
    {
        $violations = $this->validator->validate($content ?? [], new Assert\Collection([
            'purchase_price' => [new Assert\NotBlank(), new Assert\Type('integer')],
            'count' => [new Assert\NotBlank(), new Assert\Type('integer')],
            'date' => [new Assert\NotBlank(), new Assert\Type('string')],
            'supplier_id' => [new Assert\NotBlank(), new Assert\Uuid()],
            'comment' => [new Assert\Optional(new Assert\Type('string'))],
            'receipt_id' => [new Assert\Optional(new Assert\Uuid())]
        ]));

        if (0 !== count($violations)) {
            throw new ValidationException($violations);
        }
    }

    private function validateGoodsCreate(?array $content)
    {
        $violations = $this->validator->validate($content ?? [], new Assert\Collection([
            'name' => [new Assert\NotBlank(), new Assert\Type('string')],
            'short_name' => [new Assert\Optional(new Assert\Type('string'))],
            'code' => [new Assert\NotBlank(), new Assert\Type('string')],
            'is_weight' => [new Assert\NotNull(), new Assert\Type('boolean')],
            'barcode' => [new Assert\Optional(new Assert\Type('string'))],
            'price' => [new Assert\NotBlank(), new Assert\Type('integer')],
            'taxes' => [new Assert\Optional(new Assert\All(new Assert\Uuid()))],
            'group' => [new Assert\Optional(new Assert\Uuid())],
            'parent' => [new Assert\Optional(new Assert\Uuid())],
            'uktzed' => [new Assert\Optional(new Assert\Type('string'))],
            'children' => [new Assert\Optional(new Assert\All(new Assert\Uuid()))],
            'type' => [new Assert\NotBlank(), new Assert\Choice(["service", "good"])]
        ]));

        if (0 !== count($violations)) {
            throw new ValidationException($violations);
        }
    }

    private function validateGoodsWriteOffCreate(?array $content)
    {
        $violations = $this->validator->validate($content ?? [], new Assert\All(new Assert\Collection([
            'goods_id' => [new Assert\NotBlank(), new Assert\Uuid()],
            'count' => [new Assert\NotBlank(), new Assert\Type('integer')],
            'date' => [new Assert\Optional(new Assert\Type('string'))],
            'comment' => [new Assert\Optional(new Assert\Type('string'))]
        ])));

        if (0 !== count($violations)) {
            throw new ValidationException($violations);
        }
    }

    private function validateGoodsUpdate(?array $content)
    {
        $violations = $this->validator->validate($content ?? [], new Assert\Collection([
            'name' => [new Assert\Optional(new Assert\Type('string'))],
            'short_name' => [new Assert\Optional(new Assert\Type('string'))],
            'code' => [new Assert\Optional(new Assert\Type('string'))],
            'is_weight' => [new Assert\Optional(new Assert\Type('boolean'))],
            'barcode' => [new Assert\Optional(new Assert\Type('string'))],
            'position' => [new Assert\Optional(new Assert\Type('integer'))],
            'uktzed' => [new Assert\Optional(new Assert\Type('string'))],
            'price' => [new Assert\Optional(new Assert\Type('integer'))],
            'group' => [new Assert\Optional(new Assert\Uuid())],
            'children' => [new Assert\Optional(new Assert\All(new Assert\Uuid()))],
            'taxes' => [new Assert\Optional(new Assert\All(new Assert\Uuid()))],
            'type' => [new Assert\Optional(new Assert\Choice(["service", "good"]))]
        ]));

        if (0 !== count($violations)) {
            throw new ValidationException($violations);
        }
    }

    private function validateGroupsCreate(?array $content)
    {
        $violations = $this->validator->validate($content ?? [], new Assert\Collection([
            'name' => [new Assert\NotBlank(), new Assert\Type('string')],
            'description' => [new Assert\Optional(new Assert\Type('string'))],
            'parent_id' => [new Assert\Optional(new Assert\Uuid())],
        ]));

        if (0 !== count($violations)) {
            throw new ValidationException($violations);
        }
    }

    private function validateGroupsUpdate(?array $content)
    {
        $violations = $this->validator->validate($content ?? [], new Assert\Collection([
            'name' => [new Assert\Optional(new Assert\Type('string'))],
            'description' => [new Assert\Optional(new Assert\Type('string'))],
            'parent_id' => [new Assert\Optional(new Assert\Uuid())]
        ]));

        if (0 !== count($violations)) {
            throw new ValidationException($violations);
        }
    }

    private function getTaxes(): array
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('t.id', 't.symbol')
            ->from(Tax::class, 't')
            ->where('t.organization = :organization')
            ->setParameter('organization', $this->getCurrentOrganization())
            ->getQuery()
            ->getResult(AbstractQuery::HYDRATE_ARRAY);
    }

    protected function fromFile($file): StreamedResponse
    {
        $response = new StreamedResponse(function() use ($file) {
            while (!feof($file)) {
                echo fread($file, 1024 * 10);
                flush();
            }
            fclose($file);
        });
        $disposition = HeaderUtils::makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            'goods.csv'
        );
        $response->headers->set('Content-Disposition', $disposition);
        $response->headers->set('Content-Type', 'application/octet-stream');

        return $response;
    }
}
