<?php
declare(strict_types=1);

namespace App\Controller\Endpoint\OrderMonobank;

use App\Component\Signature\OpenSslSignature;
use App\Component\Traits\ParameterBagAwareTrait;
use App\Controller\Endpoint\AbstractEndpointAction;
use App\Domain\Services\Monobank\MonobankApiService;
use App\Entity\Directory\OrderMonobank;
use App\Exception\Monobank\MonobankException;
use GuzzleHttp\Exception\RequestException;
use App\Domain\Services\Monobank\MonobankInfoGenerator;
use Ramsey\Uuid\Uuid;

/**
 *  @property OrderMonobank $data
 */
class CreateOrderMonobankAction extends AbstractEndpointAction
{
    use ParameterBagAwareTrait;

    private MonobankApiService $monobankApiService;
    private MonobankInfoGenerator $monobankInfoGenerator;
    private OpenSslSignature $openSslSignature;

    public function __construct(
        MonobankApiService $monobankApiService,
        MonobankInfoGenerator $monobankInfoGenerator,
        OpenSslSignature $openSslSignature
    ) {
        $this->monobankInfoGenerator = $monobankInfoGenerator;
        $this->monobankApiService = $monobankApiService;
        $this->openSslSignature = $openSslSignature;
    }

    public function execute()
    {
        $token = $this->getCurrentOrganization()->getMonobankAcquiringClientId();
        $referenceId = Uuid::uuid4()->toString();

        $sign = $this->openSslSignature->sign($referenceId);

        $paymentInfo = $this->monobankInfoGenerator->generateInfo($this->data->getReceiptBody(), $referenceId);

        try {
            $response = $this->monobankApiService->createInvoice($paymentInfo, $token, $sign);
        } catch (RequestException $exception) {
            throw new MonobankException($exception->getMessage());
        }

        $this->data->setTotalSum($paymentInfo['amount'])
            ->setInvoiceId($response['invoiceId'])
            ->setPageUrl($response['pageUrl'])
            ->setReferenceId($referenceId);


        return $this->data;
    }
}
